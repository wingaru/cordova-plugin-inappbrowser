//
//  WKWebView+InAppWKWebView.m
//  ClassHive Kids
//
//  Created by Warren Layson on 11/24/22.
//

#import "WKWebView+InAppWKWebView.h"

@implementation WKWebView (InAppWKWebView)
-(void)useDesktopMode:(BOOL)enabled :(NSString *)useragent
{
    WKWebViewConfiguration* configuration = [[WKWebViewConfiguration alloc] init];
    
    
    if (@available(iOS 13.0, *)) {
       
        if (enabled) {
            NSLog(@"Desktop mode enabled");
            configuration.defaultWebpagePreferences.preferredContentMode = WKContentModeDesktop;
        } else {
            configuration.defaultWebpagePreferences.preferredContentMode = WKContentModeMobile;
        }
    }
    
    if ([useragent length] > 0) {
        self.customUserAgent = [self stringFromHexString:useragent];
    }
}
// taken from https://stackoverflow.com/questions/18827699/convert-hex-to-ascii-number-on-objective-c
-(NSString *)stringFromHexString:(NSString *)hexString {
    
    // The hex codes should all be two characters.
    if (([hexString length] % 2) != 0)
        return nil;
    
    NSMutableString *string = [NSMutableString string];
    
    for (NSInteger i = 0; i < [hexString length]; i += 2) {
        
        NSString *hex = [hexString substringWithRange:NSMakeRange(i, 2)];
        NSInteger decimalValue = 0;
        sscanf([hex UTF8String], "%x", &decimalValue);
        [string appendFormat:@"%c", decimalValue];
    }
    return string;
}
@end

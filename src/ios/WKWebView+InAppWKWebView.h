//
//  WKWebView+InAppWKWebView.h
//  ClassHive Kids
//
//  Created by Warren Layson on 11/24/22.
//

#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WKWebView (InAppWKWebView)
-(void) useDesktopMode:(BOOL)enabled: (NSString *) useragent;
- (NSString *)stringFromHexString:(NSString *)hexString;
@end

NS_ASSUME_NONNULL_END

package org.apache.cordova.inappbrowser;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.apache.cordova.LOG;

/**
 * @author Warren Layson
 * Created 11/25/2022 at 8:25 AM
 */
public class InAppWebView extends WebView {

    protected static final String LOG_TAG = "InAppWebView";

    public InAppWebView(Context context) {
        super(context);
    }

    public InAppWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InAppWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setDesktopMode(final boolean enabled, final @Nullable String userAgent) {
        final WebSettings settings = getSettings();

        // ? Should we still override user-agent here when we're passing its value?
        if (userAgent != null) {
            Log.d(LOG_TAG, "Overridden user-agent");
            settings.setUserAgentString(this.hexToAscii(userAgent));
        }

        settings.setUseWideViewPort(enabled);
        settings.setLoadWithOverviewMode(enabled);
        settings.setSupportZoom(enabled);
        settings.setBuiltInZoomControls(enabled);
    }

    private @NonNull String hexToAscii(@NonNull String str) {
        StringBuilder output = new StringBuilder("");

        for (int i = 0; i < str.length(); i +=2) {
            String string = str.substring(i, i+2);
            output.append((char) Integer.parseInt(string, 16));
        }

        return output.toString();
    }

}
